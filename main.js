const getElement = (id) => document.getElementById(id)

const innerContent = (id, content) => getElement(id).innerText = content

const getDoiTuong = (object1) => object1 == 'A' ? 2 : object1 == 'B' ? 1 : 0.5
const getKhuVuc = (area1) => area1 == 1 ? 2.5 : area1 == 2 ? 1.5 : 1

getElement('btn-diem').addEventListener("click", () => {
    let diem1 = Number(getElement('diem1').value),
        diem2 = Number(getElement('diem2').value),
        diem3 = Number(getElement('diem3').value),
        diemChuan = Number(getElement('diemChuan').value),
        object1 = getElement('object1').value,
        area1 = getElement('area1').value,
        diem = 0, pass = '', check = false

    if (diem1 == 0 || diem2 == 0 || diem3 == 0) {
        alert('Rớt rồi coi điểm làm gì nữa')
        return
    }

    check = (object1 != '') & (area1 != '') & (diemChuan > 0 && diemChuan <= 30) &
        (diem1 != '') & (diem2 != '') & (diem3 != '')

    if (check) {
        diem = getDoiTuong(object1) + getKhuVuc(area1) + (diem1 + diem2 + diem3)

        if (diem >= diemChuan) pass = 'Đậu'
        else pass = 'rớt'

        innerContent('result-diem', `Bạn đã ${pass} . Điểm của bạn là: ${diem}`)
    } else
        alert('Nhập thiếu dữ liệu')
})

const loai1 = 500,
    loai2 = 650,
    loai3 = 850,
    loai4 = 1100,
    loai5 = 1300

getElement('btn-electro').addEventListener("click", () => {
    let name = getElement('name').value,
        kw = Number(getElement('kw').value),
        tien = 0
    kw >= 0 && kw <= 50 ?
        tien = kw * loai1 : kw > 50 && kw <= 100 ?
            tien = 50 * loai1 + (kw - 50) * loai2 : kw > 100 && kw <= 200 ?
                tien = 50 * (loai1 + loai2) + (kw - 100) * loai3 : kw > 200 && kw <= 350 ?
                    tien = 50 * (loai1 + loai2) + 100 * loai3 + (kw - 200) * loai4 :
                    tien = 50 * (loai1 + loai2) + 100 * loai3 + 150 * loai4 + (kw - 350) * loai5

    innerContent('result-electro', `Tiền điện tháng này của bạn ${name} là: ${tien}`)
})

getElement('btn-tax').addEventListener("click", () => {
    let name = getElement('name1').value,
        total = getElement('total').value,
        people = getElement('people').value,
        tax = 0,
        net = total - 4e6 - 16e5 * people

    if (name.trim() === '' || total * people == 0) {
        alert('Dữ liệu nhập sai')
        return
    }
    net > 0 && net <= 6e7 ? tax = net * 0.05
        : net > 6e7 && net <= 12e7 ? tax = net * 0.1
            : net > 12e7 && net <= 21e7 ? tax = net * 0.15
                : net > 21e7 && net < 384e6 ? tax = net * 0.2
                    : net > 384e6 && net <= 624e6 ? tax = net * 0.25
                        : net > 624e6 && net <= 96e7 ? tax = net * 0.3
                            : net > 96e7 ? tax = net * 0.35 : tax = 0
    tax === 0 ? alert('Trốn thuế hả') : innerContent('result-tax', `${name}
    Thuế thu nhập cá nhân của bạn: ${tax.toLocaleString()} vnđ`)
})

getElement('customer').addEventListener('change', (e) => {
    let company = e.currentTarget.value;
    getElement('connect').style.display = company == 'N' ? "block" : "none";
})

tienCap = (customer, kenh, connect) => {
    return customer == 'D' ? (4.5 + 20.5 + 7.5 * kenh)
        : (15 + (connect > 10 ? 75 + (connect - 10) * 5 : 75) + 50 * kenh)
}

getElement('btn-cap').addEventListener('click', () => {
    let customer = getElement('customer').value,
        id = getElement('id').value,
        kenh = getElement('kenh').value,
        connect = getElement('connect').value,
        tong = tienCap(customer, kenh, connect)
    console.log(customer, id, kenh, connect)
    customer.trim() == '' || id.trim() == '' || kenh.trim() == ''
        ? alert('Nhập thiếu') : innerContent('result-cap', `Mã khách hàng: ${id}
                                                            Tiền cáp: $${tong}`)
})


